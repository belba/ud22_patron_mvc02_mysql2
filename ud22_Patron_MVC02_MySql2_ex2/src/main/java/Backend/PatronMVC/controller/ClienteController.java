package Backend.PatronMVC.controller;

import Backend.PatronMVC.model.dto.Cliente;
import Backend.PatronMVC.model.service.ClienteService;
import Backend.PatronMVC.view.VentanaBuscarCliente;
import Backend.PatronMVC.view.VentanaPrincipal;
import Backend.PatronMVC.view.VentanaRegistroCliente;

public class ClienteController {
	private ClienteService clienteService;
	private VentanaPrincipal miVentanaPrincipal;
	private VentanaRegistroCliente miVentanaRegistro;
	private VentanaBuscarCliente miVentanaBuscar;
	
	
	/**
	 * @return the miVentanaPrincipal
	 */
	public VentanaPrincipal getMiVentanaPrincipal() {
		return miVentanaPrincipal;
	}
	/**
	 * @param miVentanaPrincipal the miVentanaPrincipal to set
	 */
	public void setMiVentanaPrincipal(VentanaPrincipal miVentanaPrincipal) {
		this.miVentanaPrincipal = miVentanaPrincipal;
	}
	/**
	 * @return the miVentanaRegistro
	 */
	public VentanaRegistroCliente getMiVentanaRegistro() {
		return miVentanaRegistro;
	}
	/**
	 * @param miVentanaRegistro the miVentanaRegistro to set
	 */
	public void setMiVentanaRegistro(VentanaRegistroCliente miVentanaRegistro) {
		this.miVentanaRegistro = miVentanaRegistro;
	}
	/**
	 * @return the miVentanaBuscar
	 */
	public VentanaBuscarCliente getMiVentanaBuscar() {
		return miVentanaBuscar;
	}
	/**
	 * @param miVentanaBuscar the miVentanaBuscar to set
	 */
	public void setMiVentanaBuscar(VentanaBuscarCliente miVentanaBuscar) {
		this.miVentanaBuscar = miVentanaBuscar;
	}
	/**
	 * @return the clienteService
	 */
	public ClienteService getClienteService() {
		return clienteService;
	}
	/**
	 * @param clienteService the clienteService to set
	 */
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	public void registrarCliente(Cliente cliente) {
		clienteService.validarRegistro(cliente);
	}
	
	public Cliente buscarCliente(String codigoCliente) {
		return clienteService.validarConsulta(codigoCliente);
	}
	
	public void modificarCliente(Cliente cliente) {
		clienteService.validarModificacion(cliente);
	}
	
	public void eliminarCliente(String codigoCliente) {
		clienteService.validarEliminacion(codigoCliente);
	}
	
	//Hace visible las vistas de Registro y Consulta
	public void mostrarVentanaRegistro() {
		miVentanaRegistro.setVisible(true);
	}
	public void mostrarVentanaConsulta() {
		miVentanaBuscar.setVisible(true);
	}
	
}
