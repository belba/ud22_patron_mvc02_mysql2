package Backend.PatronMVC.controller;

import Backend.PatronMVC.model.dto.Video;
import Backend.PatronMVC.model.service.VideoService;
import Backend.PatronMVC.view.VentanaRegistroVideo;

public class VideoController {
	
	private VideoService videoService;
	private VentanaRegistroVideo ventanaRegistroVideo;

	
	/**
	 * @return the ventanaRegistroVideo
	 */
	public VentanaRegistroVideo getVentanaRegistroVideo() {
		return ventanaRegistroVideo;
	}

	/**
	 * @param ventanaRegistroVideo the ventanaRegistroVideo to set
	 */
	public void setVentanaRegistroVideo(VentanaRegistroVideo ventanaRegistroVideo) {
		this.ventanaRegistroVideo = ventanaRegistroVideo;
	}

	/**
	 * @return the videoService
	 */
	public VideoService getVideoService() {
		return videoService;
	}

	/**
	 * @param videoService the videoService to set
	 */	
	public void setVideoService(VideoService videoService) {
		this.videoService = videoService;
	}
	
	//Validamos el video y lo registramos
	public void registrarVideo(Video video) {
		videoService.validarRegistro(video);
	}
	
	//Hacer visible las vistas de Registro de videos y consulta
	public void mostrarVentanaRegistroVideo() {
		ventanaRegistroVideo.setVisible(true);
	}
	
}
