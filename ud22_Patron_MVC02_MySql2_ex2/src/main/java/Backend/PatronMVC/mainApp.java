package Backend.PatronMVC;

import Backend.PatronMVC.controller.ClienteController;
import Backend.PatronMVC.controller.VideoController;
import Backend.PatronMVC.model.service.ClienteService;
import Backend.PatronMVC.model.service.VideoService;
import Backend.PatronMVC.view.VentanaBuscarCliente;
import Backend.PatronMVC.view.VentanaPrincipal;
import Backend.PatronMVC.view.VentanaRegistroCliente;
import Backend.PatronMVC.view.VentanaRegistroVideo;

public class mainApp {
	
	ClienteService clienteService;
	VideoService videoService;
	
	VentanaPrincipal miVentanaPrincipal;
	VentanaBuscarCliente miVentanaBuscar;
	VentanaRegistroCliente miVentanaRegistro;
	
	VentanaRegistroVideo miVentanaRegistroVideo;
	
	
	ClienteController clienteController;
	VideoController videoController;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		mainApp miPrincipal=new mainApp();
		miPrincipal.iniciar();
	}

	/**
	 * Permite instanciar todas las clases con las que trabaja
	 * el sistema
	 */
	private void iniciar() {
		/*Se instancian las clases*/
		miVentanaPrincipal=new VentanaPrincipal();
		miVentanaRegistro=new VentanaRegistroCliente();
		miVentanaBuscar= new VentanaBuscarCliente();
		miVentanaRegistroVideo = new VentanaRegistroVideo();
			
		clienteService=new ClienteService();
		videoService= new VideoService();
		
		clienteController = new ClienteController();
		videoController = new VideoController();
		
		/*Se establecen las relaciones entre clases*/
		miVentanaPrincipal.setClienteController(clienteController);
		miVentanaPrincipal.setVideoController(videoController);
		miVentanaRegistro.setClienteController(clienteController);
		miVentanaBuscar.setClienteController(clienteController);
		miVentanaRegistroVideo.setVideoController(videoController);
		
		clienteService.setClienteController(clienteController);
		videoService.setVideoController(videoController);
		
		/*Se establecen relaciones con la clase coordinador*/
		clienteController.setMiVentanaPrincipal(miVentanaPrincipal);
		clienteController.setMiVentanaRegistro(miVentanaRegistro);
		clienteController.setMiVentanaBuscar(miVentanaBuscar);
		videoController.setVentanaRegistroVideo(miVentanaRegistroVideo);
		
		clienteController.setClienteService(clienteService);
		videoController.setVideoService(videoService);
		
		miVentanaPrincipal.setVisible(true);
	}

}
