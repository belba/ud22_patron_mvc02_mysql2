package Backend.PatronMVC.model.service;

import javax.swing.JOptionPane;

import Backend.PatronMVC.controller.VideoController;
import Backend.PatronMVC.model.dao.VideoDao;
import Backend.PatronMVC.model.dto.Video;

public class VideoService {

	private VideoController videoController;
	public static boolean consultarVideo=false;
	public static boolean modificaVideo=false;
	

	public void validarRegistro(Video video) {
		VideoDao videoDao;
		
		if(!video.getId().equals(null) &&  !video.getId().equals("")) {
			if(!video.getIdCliente().equals(null) && !video.getIdCliente().equals("")) {
				
				videoDao = new VideoDao();
				videoDao.registrarVideo(video);
			}else {
				JOptionPane.showMessageDialog(null,"id del cliente no puede estar vacia","Advertencia",JOptionPane.WARNING_MESSAGE);
				
			}
		}else {
			JOptionPane.showMessageDialog(null,"id del video no puede estar vacia","Advertencia",JOptionPane.WARNING_MESSAGE);
			
		}
		
				
	}


	/**
	 * @return the videoController
	 */
	public VideoController getVideoController() {
		return videoController;
	}


	/**
	 * @param videoController the videoController to set
	 */
	public void setVideoController(VideoController videoController) {
		this.videoController = videoController;
	}
	
	

}
