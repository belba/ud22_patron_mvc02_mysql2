package Backend.PatronMVC.model.service;

import javax.swing.JOptionPane;

import Backend.PatronMVC.controller.ClienteController;
import Backend.PatronMVC.model.dao.ClienteDao;
import Backend.PatronMVC.model.dto.Cliente;

public class ClienteService {

	private ClienteController clienteController;
	public static boolean consultarCliente=false;
	public static boolean modificaCliente=false;
	
	public void validarRegistro(Cliente cliente) {
		ClienteDao clienteDao;
		
		if(cliente.getIdCliente() > 99) {
			clienteDao = new ClienteDao();
			clienteDao.registrarCliente(cliente);
			
		}else {
			JOptionPane.showMessageDialog(null,"El documento de la persona debe ser mas de 3 digitos","Advertencia",JOptionPane.WARNING_MESSAGE);
		}			
	}
	
	//Metodo que valida los datos de consulta antes de pasar estos al DAO
	public Cliente validarConsulta(String codigoCliente) {
		ClienteDao clienteDao;
		
		try {
			int codigo=Integer.parseInt(codigoCliente);	
			if (codigo > 1) {
				clienteDao = new ClienteDao();
				consultarCliente=true;
				return clienteDao.buscarCliente(codigo);						
			}else{
				JOptionPane.showMessageDialog(null,"El documento del cliente debe ser mas de 3 digitos","Advertencia",JOptionPane.WARNING_MESSAGE);
				consultarCliente=false;
			}
			
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
			consultarCliente=false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			consultarCliente=false;
		}
					
		return null;
	}
	
	//Metodo que valida los datos de Modificación antes de pasar estos al DAO
	public void validarModificacion(Cliente cliente) {
		ClienteDao clienteDao;
		if (cliente.getNombreCliente().length()>1) {
			clienteDao = new ClienteDao();
			clienteDao.modificarCliente(cliente);	
			modificaCliente=true;
		}else{
			JOptionPane.showMessageDialog(null,"El nombre de la persona debe ser mayor a 1 digitos","Advertencia",JOptionPane.WARNING_MESSAGE);
			modificaCliente=false;
		}
	}

	//Metodo que valida los datos de Eliminación antes de pasar estos al DAO
	public void validarEliminacion(String codigo) {
		ClienteDao clienteDao = new ClienteDao();
		clienteDao.eliminarCliente(codigo);
	}
		
	/**
	 * @return the clienteController
	 */
	public ClienteController getClienteController() {
		return clienteController;
	}
	/**
	 * @param clienteController the clienteController to set
	 */
	public void setClienteController(ClienteController clienteController) {
		this.clienteController = clienteController;
		
	}	
}
