package Backend.PatronMVC.model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import Backend.PatronMVC.model.conexion.Conexion;
import Backend.PatronMVC.model.dto.Cliente;

public class ClienteDao {
	
	private Conexion con = new Conexion();
	private Cliente cliente = new Cliente();
	
	public void registrarCliente(Cliente cliente) {
		try {
			Statement st = con.getConnection().createStatement();
			String sql = "INSERT INTO cliente VALUES ('"+cliente.getIdCliente()+"','"
					+cliente.getNombreCliente()+"','"+cliente.getApellidoCliente()+"','"
					+cliente.getDireccionCliente()+"','"+cliente.getDniCliente()+"','"
					+cliente.getFechaCliente()+"');";
			
			st.executeUpdate(sql);
			JOptionPane.showMessageDialog(null, "Se ha registrado el cliente "+ cliente.getNombreCliente() +" "+ cliente.getApellidoCliente() +" Exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
			st.close();
			con.desconectar();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "No se Registro");
		}
	}

	public Cliente buscarCliente(int codigo) {
		boolean existeCliente = false;
		
		try {
			String sql = "SELECT * FROM cliente where id= ?";
			PreparedStatement consulta = con.getConnection().prepareStatement(sql);
			consulta.setInt(1, codigo);
			ResultSet res = consulta.executeQuery();
			while(res.next()) {
				existeCliente = true;
				cliente.setIdCliente(Integer.parseInt(res.getString("id")));
				cliente.setNombreCliente(res.getString("nombre"));
				cliente.setApellidoCliente(res.getString("apellido"));
				cliente.setDireccionCliente(res.getString("direccion"));
				cliente.setDniCliente(Integer.parseInt(res.getString("dni")));
				cliente.setFechaCliente(res.getString("fecha"));
			}
			res.close();
			con.desconectar();
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Error, no se conecto");
		}
		
		if (existeCliente) {
			return cliente;
		}
		else return null;	
		
	}
	
	public void modificarCliente(Cliente cliente) {
		try {
			String consulta="UPDATE cliente SET id= ? ,nombre = ? ,apellido= ? ,direccion= ? ,dni= ?, fecha=? WHERE id= ? ";
			PreparedStatement estadoConsulta = con.getConnection().prepareStatement(consulta);
			
			estadoConsulta.setInt(1, cliente.getIdCliente());
			estadoConsulta.setString(2, cliente.getNombreCliente());
			estadoConsulta.setString(3, cliente.getApellidoCliente());
			estadoConsulta.setString(4, cliente.getDireccionCliente());
			estadoConsulta.setInt(5, cliente.getDniCliente());
			estadoConsulta.setString(6, cliente.getFechaCliente());
			estadoConsulta.setInt(7, cliente.getIdCliente());
			estadoConsulta.executeUpdate();
			JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ","Confirmación",JOptionPane.INFORMATION_MESSAGE);
			
		} catch (SQLException e) {
			 JOptionPane.showMessageDialog(null, "Error al Modificar","Error",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void eliminarCliente(String idCliente) {
		
		try {
			String sql= "DELETE FROM cliente WHERE id='"+idCliente+"'";
			Statement st = con.getConnection().createStatement();
			st.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
            System.out.println(sql);
			st.close();
			con.desconectar();
		} catch (SQLException e) {
			// TODO: handle exception
			JOptionPane.showMessageDialog(null, "Error al elimianar cliente");
		}
		
	}	
}
