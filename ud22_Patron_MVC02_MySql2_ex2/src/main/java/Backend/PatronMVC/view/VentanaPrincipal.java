package Backend.PatronMVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import Backend.PatronMVC.controller.ClienteController;
import Backend.PatronMVC.controller.VideoController;

public class VentanaPrincipal extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	private ClienteController clienteController;
	private VideoController videoController;
	private JLabel labelTitulo, labelSeleccion;
	private JButton botonRegistrar,botonBuscar,btnRegistrarVideo;
	
	

	/**
	 * @param videoController the videoController to set
	 */
	public void setVideoController(VideoController videoController) {
		this.videoController = videoController;
	}

	/**
	 * @param clienteController the clienteController to set
	 */
	public void setClienteController(ClienteController clienteController) {
		this.clienteController = clienteController;
	}


	/**
	 * Establece la informacion que se presentara como introduccion del sistema
	 */
	public String textoIntroduccion = "";
	
	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana principal
	 */
	public VentanaPrincipal() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		botonRegistrar = new JButton();
		botonRegistrar.setBounds(40, 125, 140, 25);
		botonRegistrar.setText("Registrar Cliente");
		
		botonBuscar = new JButton();
		botonBuscar.setBounds(190, 125, 120, 25);
		botonBuscar.setText("Buscar Cliente");
				
		btnRegistrarVideo = new JButton();
		btnRegistrarVideo.setText("Registrar video");
		btnRegistrarVideo.setBounds(320, 125, 120, 25);

		labelTitulo = new JLabel();
		labelTitulo.setText("PATRON MODELO VISTA CONTROLADOR");
		labelTitulo.setBounds(60, 40, 380, 30);
		labelTitulo.setFont(new java.awt.Font("Verdana", 1, 15));

		labelSeleccion = new JLabel();
		labelSeleccion.setText("Escoja que operacion desea realizar");
		labelSeleccion.setBounds(60, 92, 250, 25);

		botonRegistrar.addActionListener(this);
		botonBuscar.addActionListener(this);
		btnRegistrarVideo.addActionListener(this);
		
		getContentPane().add(botonBuscar);
		getContentPane().add(botonRegistrar);
		getContentPane().add(labelSeleccion);
		getContentPane().add(btnRegistrarVideo);
		getContentPane().add(labelTitulo);
		
		setSize(496, 206);
		setTitle("Patron de Diseño/MVC");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==botonRegistrar) {
			clienteController.mostrarVentanaRegistro();			
		}
		if (e.getSource()==botonBuscar) {
			clienteController.mostrarVentanaConsulta();			
		}
		if (e.getSource()==btnRegistrarVideo) {
			videoController.mostrarVentanaRegistroVideo();			
		}
	}
}
