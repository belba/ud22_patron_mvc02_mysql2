package Backend.PatronMVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Backend.PatronMVC.controller.ClienteController;
import Backend.PatronMVC.controller.VideoController;
import Backend.PatronMVC.model.dto.Cliente;
import Backend.PatronMVC.model.dto.Video;


public class VentanaRegistroVideo extends JFrame implements ActionListener{

	
	private static final long serialVersionUID = 1L;
	private VideoController videoController;
	
	private JLabel labelTitulo;
	private JTextField textCodVideo,textTitle,textDirector, textCodiClient;
	private JLabel nombre,direccion,txtCod_2, cod_1;	
	private JButton botonGuardar,botonCancelar;
	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana de registro
	 */
	public VentanaRegistroVideo() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(81, 220, 120, 25);
		botonGuardar.setText("Guardar");
		
		botonCancelar = new JButton();
		botonCancelar.setBounds(248, 220, 120, 25);
		botonCancelar.setText("Cancelar");

		labelTitulo = new JLabel();
		labelTitulo.setText("REGISTRO DE VIDEOS");
		labelTitulo.setBounds(120, 20, 253, 30);
		labelTitulo.setFont(new java.awt.Font("Verdana", 1, 18));
		
		nombre=new JLabel();
		nombre.setText("Title");
		nombre.setBounds(20, 120, 80, 25);
		getContentPane().add(nombre);
		
		direccion=new JLabel();
		direccion.setText("Director");
		direccion.setBounds(20, 160, 80, 25);
		getContentPane().add(direccion);
		
		textCodVideo=new JTextField();
		textCodVideo.setBounds(100, 80, 80, 25);
		getContentPane().add(textCodVideo);
		
		textTitle=new JTextField();
		textTitle.setBounds(80, 120, 190, 25);
		getContentPane().add(textTitle);
		
		textDirector=new JTextField();
		textDirector.setBounds(80, 160, 190, 25);
		getContentPane().add(textDirector);
		
		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);
		getContentPane().add(botonCancelar);
		getContentPane().add(botonGuardar);
		getContentPane().add(labelTitulo);

		cod_1 = new JLabel();
		cod_1.setText("Codigo Cliente");
		cod_1.setBounds(212, 80, 91, 25);
		getContentPane().add(cod_1);
		
		textCodiClient = new JTextField();
		textCodiClient.setText("");
		textCodiClient.setBounds(313, 80, 80, 25);
		getContentPane().add(textCodiClient);
		
		txtCod_2 = new JLabel();
		txtCod_2.setText("Codigo Video");
		txtCod_2.setBounds(20, 80, 91, 25);
		getContentPane().add(txtCod_2);
		
		
		limpiar();
		setSize(428, 305);
		setTitle("Patron de Diseño/MVC");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
		

	}


	private void limpiar() {
		textCodVideo.setText("");
		textDirector.setText("");
		textTitle.setText("");
		textCodiClient.setText("");
	}


	/**
	 * @param videoController the videoController to set
	 */
	public void setVideoController(VideoController videoController) {
		this.videoController = videoController;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource()==botonGuardar) {
			try {				
				Video video = new Video();
				video.setDirector(textDirector.getText());
				video.setTitle(textTitle.getText());
				video.setIdCliente(Integer.parseInt(textCodiClient.getText()));
				video.setId(Integer.parseInt(textCodVideo.getText()));
				
				videoController.registrarVideo(video);
				
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
				System.out.println(ex);
			}
		}
		if (e.getSource()==botonCancelar)
		{
			this.dispose();
		}
	}
}
