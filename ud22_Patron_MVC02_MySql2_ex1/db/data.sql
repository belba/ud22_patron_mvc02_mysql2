
CREATE DATABASE IF NOT EXISTS `UD22_MVC_db_CRUD_YASSINE`;
USE `UD22_MVC_db_CRUD_YASSINE`;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `apellido` varchar(250) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `dni` int DEFAULT NULL,
  `fecha` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
