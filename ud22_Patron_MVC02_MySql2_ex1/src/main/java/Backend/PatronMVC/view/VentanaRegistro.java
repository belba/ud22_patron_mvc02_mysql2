package Backend.PatronMVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Backend.PatronMVC.controller.ClienteController;
import Backend.PatronMVC.model.dto.Cliente;


public class VentanaRegistro extends JFrame implements ActionListener{

	
	private static final long serialVersionUID = 1L;
	private ClienteController clienteController;
	
	private JLabel labelTitulo;
	private JTextField textCod,textNombre,textapellido,textdireccion,textdni,textfecha;
	private JLabel cod,nombre,apellido,direccion,dni,fecha;	
	private JButton botonGuardar,botonCancelar;
	
	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana de registro
	 */
	public VentanaRegistro() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(110, 220, 120, 25);
		botonGuardar.setText("Guardar");
		
		botonCancelar = new JButton();
		botonCancelar.setBounds(250, 220, 120, 25);
		botonCancelar.setText("Cancelar");

		labelTitulo = new JLabel();
		labelTitulo.setText("REGISTRO DE CLIENTES");
		labelTitulo.setBounds(120, 20, 380, 30);
		labelTitulo.setFont(new java.awt.Font("Verdana", 1, 18));

		cod=new JLabel();
		cod.setText("Codigo");
		cod.setBounds(20, 80, 80, 25);
		getContentPane().add(cod);
		
		nombre=new JLabel();
		nombre.setText("Nombre");
		nombre.setBounds(20, 120, 80, 25);
		getContentPane().add(nombre);

		apellido=new JLabel();
		apellido.setText("Apellido");
		apellido.setBounds(290, 160, 80, 25);
		getContentPane().add(apellido);
		
		direccion=new JLabel();
		direccion.setText("Dirección");
		direccion.setBounds(20, 160, 80, 25);
		getContentPane().add(direccion);
		
		dni=new JLabel();
		dni.setText("DNI");
		dni.setBounds(290, 120, 80, 25);
		getContentPane().add(dni);		

		fecha= new JLabel();
		fecha.setText("Fecha");
		fecha.setBounds(290, 80, 40, 25);
		getContentPane().add(fecha);
		
		textCod=new JTextField();
		textCod.setBounds(80, 80, 80, 25);
		getContentPane().add(textCod);
		
		textNombre=new JTextField();
		textNombre.setBounds(80, 120, 190, 25);
		getContentPane().add(textNombre);

		textapellido=new JTextField();
		textapellido.setBounds(340, 160, 80, 25);
		getContentPane().add(textapellido);
		
		textdireccion=new JTextField();
		textdireccion.setBounds(80, 160, 190, 25);
		getContentPane().add(textdireccion);
		
		textdni=new JTextField();
		textdni.setBounds(340, 120, 80, 25);
		getContentPane().add(textdni);
		

		textfecha = new JTextField();
		textfecha.setBounds(340, 82, 80, 20);
		getContentPane().add(textfecha);
		textfecha.setColumns(10);
		
		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);
		getContentPane().add(botonCancelar);
		getContentPane().add(botonGuardar);
		getContentPane().add(labelTitulo);
		limpiar();
		setSize(480, 300);
		setTitle("Patron de Diseño/MVC");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);

	}


	private void limpiar() {
		textCod.setText("");
		textapellido.setText("");
		textdireccion.setText("");
		textfecha.setText("");
		textNombre.setText("");
		textdni.setText("");
	}


	/**
	 * @param clienteController the clienteController to set
	 */
	public void setClienteController(ClienteController clienteController) {
		this.clienteController = clienteController;
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource()==botonGuardar)
		{
			try {
				Cliente cliente = new Cliente();
				cliente.setIdCliente(Integer.parseInt(textCod.getText()));
				cliente.setNombreCliente(textNombre.getText());
				cliente.setApellidoCliente(textapellido.getText());
				cliente.setDireccionCliente(textdireccion.getText());
				cliente.setDniCliente(Integer.parseInt(textdni.getText()));
				cliente.setFechaCliente(textfecha.getText());
				
				clienteController.registrarCliente(cliente);				
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
				System.out.println(ex);
			}
		}
		if (e.getSource()==botonCancelar)
		{
			this.dispose();
		}
	}
	
	

}
